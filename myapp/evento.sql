-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 09-Maio-2019 às 21:33
-- Versão do servidor: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `evento`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `evento`
--

DROP TABLE IF EXISTS `evento`;
CREATE TABLE IF NOT EXISTS `evento` (
  `eve_tipo` varchar(200) NOT NULL,
  `eve_data` date NOT NULL,
  `eve_local` varchar(200) NOT NULL,
  `eve_ingresso` varchar(50) DEFAULT NULL,
  `eve_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `eve_nome` varchar(200) NOT NULL,
  PRIMARY KEY (`eve_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `inscricao`
--

DROP TABLE IF EXISTS `inscricao`;
CREATE TABLE IF NOT EXISTS `inscricao` (
  `ins_codigo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ins_codigo_evento` int(11) NOT NULL,
  `ins_codigo_usuario` int(11) NOT NULL,
  PRIMARY KEY (`ins_codigo`),
  UNIQUE KEY `ins_codigo` (`ins_codigo`),
  KEY `fk_ins_codigo_evento` (`ins_codigo_evento`),
  KEY `fk_ins_codigo_usuario` (`ins_codigo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `usu_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `usu_nome` varchar(200) NOT NULL,
  `usu_cpf` varchar(20) NOT NULL,
  `usu_telefone` varchar(30) DEFAULT NULL,
  `usu_datanascimento` date DEFAULT NULL,
  `usu_tipo` int(11) NOT NULL,
  `usu_senha` varchar(200) NOT NULL,
  PRIMARY KEY (`usu_codigo`),
  UNIQUE KEY `usu_codigo` (`usu_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `inscricao`
--
ALTER TABLE `inscricao`
  ADD CONSTRAINT `fk_ins_codigo_evento` FOREIGN KEY (`ins_codigo_evento`) REFERENCES `evento` (`eve_codigo`),
  ADD CONSTRAINT `fk_ins_codigo_usuario` FOREIGN KEY (`ins_codigo_usuario`) REFERENCES `usuario` (`usu_codigo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
