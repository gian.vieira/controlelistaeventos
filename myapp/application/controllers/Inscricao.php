<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inscricao extends MY_Controller {


    public function inscrever($codigo1=null, $codigo2=null)
    {
        $this->load->model('inscricao_model');
        $this->load->model('evento_model');

        $dados['inscricao'] = $this->inscricao_model->inserirUsuarioEvento($codigo1, $codigo2);
        $dados['nomeEvento'] = $this->evento_model->listaUnicoEvento($codigo1);        
        $dados['nome'] = $this->session->userdata('nome');         
        
        $chat_id='837857038';

        $token='812544987:AAFO4tAgCoQn31P-nhGijmdzkjsFNGneYA4';

        $mensagem = $dados['nome'] . ' cadastrou-se no evento ' . $dados['nomeEvento'][0]->eve_nome;
        $request_params = [
            'chat_id' => $chat_id,
            'text' => $mensagem,
        ];

        $url = 'https://api.telegram.org/bot'. $token .'/sendMessage?'. http_build_query($request_params);

        $execucao = file_get_contents($url);


        redirect('modalinsricao');


    }

    public function listarInscricoes()
    {
        $this->load->model('inscricao_model');
		$dados = [
					'opcoes_inscricao' => $this->inscricao_model->tabelaEvento()
        ];
        $this->load->view('inscricoes', $dados);
    }

}