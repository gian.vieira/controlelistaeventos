<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modal extends MY_Controller {


    public function modalEventoCadastro()
    {
        $this->load->view('modal_evento_cadastro');
    }

    public function modalEventoAtualizacao()
    {
        $this->load->view('modal_evento_atualizacao');
    }

    public function modalUsuarioCadastro()
    {
        $this->load->view('modal_usuario_cadastro');
    }

    public function modalUsuarioAtulizacao()
    {
        $this->load->view('modal_usuario_atualizacao');
    }

    public function modalInscricao()
    {
        $this->load->view('modal_inscricao');
    }

}

