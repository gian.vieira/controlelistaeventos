<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
	}

	public function index()
	{
		
		$this->load->view('login');
	}

	public function validar()
	{	
		if($this->session->has_userdata('usuario'))
		{
			redirect('inscricoes');	
		}
		else
		{
			if ($this->login_model->validar())
			{
				redirect('inscricoes');
			}
			else
			{
				redirect('login');
			}

		}
		
	}

	public function logout()
	{
		$this->login_model->destroi_sessao();
		redirect('login');
	}

	public function registre()
    {
        $this->load->view('register');
	}
	
	public function inserirRegistre()
    {
      $dados['usu_nome'] = $this->input->post('nome');
			$dados['usu_cpf'] = $this->input->post('cpf');
			$dados['usu_telefone'] = $this->input->post('telefone');
			$dados['usu_datanascimento'] = $this->input->post('data');
			$dados['usu_datanascimento'] = implode("-", array_reverse(explode("/", $dados['usu_datanascimento'])));
			$dados['usu_tipo'] = 2;
			$dados['usu_senha'] = $this->input->post('senha');
			
			$this->load->model('usuario_model');

					if($this->usuario_model->inserir($dados))
					{
							redirect('login');
					};
    }
}
