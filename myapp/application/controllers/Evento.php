<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evento extends MY_Controller {

    
	public function novo()
	{		
		$this->load->view('crud_eventos');		
    }
        
    public function lista()
    {
        $this->load->view('listar_eventos'); 
    }

    public function inscricoes()
    {
        $this->load->view('inscricoes');
    }

    public function inserirEvento()
    {
        $dados['eve_nome'] = $this->input->post('nome');
		$dados['eve_tipo'] = $this->input->post('tipo');
		$dados['eve_local'] = $this->input->post('local');
		$dados['eve_data'] = $this->input->post('data');
		$dados['eve_data'] = implode("-", array_reverse(explode("/", $dados['eve_data'])));
		$dados['eve_ingresso'] = $this->input->post('ingresso');
		
		$this->load->model('evento_model');

        if($this->evento_model->inserir($dados))
        {
			
			redirect('modaleventocadastro');
        };
    }

    public function atualizarEvento()
	{
		
		$dados['eve_codigo'] = $this->input->post('codigo');
		$dados['eve_nome'] = $this->input->post('nome');
		$dados['eve_tipo'] = $this->input->post('tipo');
		$dados['eve_local'] = $this->input->post('local');
        $dados['eve_data'] = $this->input->post('data');
        $dados['eve_data'] = implode("-", array_reverse(explode("/", $dados['eve_data'])));
		$dados['eve_ingresso'] = $this->input->post('ingresso');		
		

		$this->load->model('evento_model');

		if ($this->evento_model->atualizar($dados))
		{
            redirect('modaleventoatualiza');
        }

	}

    public function listarEventos()
    {
        $this->load->model('evento_model');
		$dados = [
					'opcoes_eventos' => $this->evento_model->tabelaEvento()
		];
				
		$this->load->view('listareventos', $dados);
    }

    public function alterarEvento($codigo=null)
	{
		
		 
		$this->load->model('evento_model');	

		
		$dados['eventos'] = $this->evento_model->selecionaevento($codigo);	
	
		$this->load->view('alterareventos', $dados);
	}

	
	public function listarEventosInscritos()
    {
        $this->load->model('evento_model');
		$dados = [
					'opcoes_eventos' => $this->evento_model->tabelaEventoInscrito()
		];
				
		$this->load->view('listareventosinscritos', $dados);
    }

    
	
}