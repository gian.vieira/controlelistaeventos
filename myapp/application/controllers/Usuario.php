<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends MY_Controller {		

	
	public function novo()
	{
		$this->load->view('crud_usuarios');
	}

	public function listarUsuarios()
    {
        $this->load->model('usuario_model');
		$dados = [
					'opcoes_usuario' => $this->usuario_model->tabelaUsuario()
		];
				
		$this->load->view('listar_usuarios', $dados);
    }


	public function inserirUsuario()
    {
      $dados['usu_nome'] = $this->input->post('nome');
			$dados['usu_cpf'] = $this->input->post('cpf');
			$dados['usu_telefone'] = $this->input->post('telefone');
			$dados['usu_datanascimento'] = $this->input->post('data');
			$dados['usu_datanascimento'] = implode("-", array_reverse(explode("/", $dados['usu_datanascimento'])));
			$dados['usu_tipo'] = $this->input->post('tipo');
			$dados['usu_senha'] = $this->input->post('senha');
			
			$this->load->model('usuario_model');

					if($this->usuario_model->inserir($dados))
					{
							redirect('modalusuariocadastro');
					};
		}
		
	
	public function atualizarUsuario()
	{
			$dados['usu_codigo'] = $this->input->post('codigo');		
			$dados['usu_nome'] = $this->input->post('nome');
			$dados['usu_cpf'] = $this->input->post('cpf');
			$dados['usu_telefone'] = $this->input->post('telefone');
			$dados['usu_datanascimento'] = $this->input->post('data');
			$dados['usu_datanascimento'] = implode("-", array_reverse(explode("/", $dados['usu_datanascimento'])));
			$dados['usu_tipo'] = $this->input->post('tipo');
			$dados['usu_senha'] = $this->input->post('senha');
			
			$this->load->model('usuario_model');

					if($this->usuario_model->atualizar($dados))
					{
							redirect('modalusuarioatualiza');
					};
	}


	public function alterarUsuario($codigo=null)
	{
		
		 
		$this->load->model('usuario_model');	

		
		$dados['usuarios'] = $this->usuario_model->selecionausuario($codigo);	
	
		$this->load->view('alterarusuarios', $dados);
	}
    
    
    }
