<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url() ?>application/views/lib/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="<?= base_url() ?>application/views/lib/css/sb-admin.css" rel="stylesheet">

</head>

<body class="bg-dark">

  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
      <div class="card-body">
        <form method="post" action="logar">
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" name="cpf" class="form-control" placeholder="Digite seu CPF" required="required" autofocus="autofocus">
              <label for="cpf">CPF</label>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input type="password" name="senha" class="form-control" placeholder="Digite sua senha" required="required">
              <label for="senha">Senha</label>
            </div>
          </div>          
          <button type="submit" class="btn btn-primary btn-block">Acessar</button>
          <?php
          if(isset($_SESSION['login_erro']))
          {
            ?><div class="alert alert text-center" role="alert">
              <?php echo $_SESSION['login_erro'];
              unset($_SESSION['login_erro']);
            } ?>
        </form>
        <div class="text-center">
          <a class="d-block small" href="registro">Cadastrar-se</a>
          
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url() ?>application/views/lib/js/jquery.min.js"></script>
  <script src="<?= base_url() ?>application/views/lib/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
