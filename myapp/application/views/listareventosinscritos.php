<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Cadastro de Eventos</title>

  <!-- Custom fonts for this template-->
  <link href="lib/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="lib/css/sb-admin.css" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="inscricoes">Eventos</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>


    
  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Eventos</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <a class="dropdown-item" href="novoevento">Novo Evento</a>
          <a class="dropdown-item" href="listarevento">Listar Eventos</a>
          <a class="dropdown-item" href="inscricoes  ">Inscrições</a>
          <a class="dropdown-item" href="meuseventos">Meus Eventos</a>          
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Usuários</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <a class="dropdown-item" href="novousuario">Novo Usuário</a>
          <a class="dropdown-item" href="listarusuario">Listar Usuários</a>
        </div>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="deslogar">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Logout</span>
        </a>
      </li>  
    </ul>

    <div id="content-wrapper">

      <div class="container-fluid">
        <h3>Meus Eventos</h3>
        <table class="table">
          <thead>
            <th>NOME DO EVENTO</th>
            <th>DATA</th>
           
            
          </thead>
          <tbody>
            <?php  echo $opcoes_eventos ?>
          </tbody>
        </table>
      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Gerenciador de Eventos 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>

  <!-- Modal -->

  <!-- /#wrapper -->
   
<script>
  $(document).ready(function (){
    $('myModal').modal('show');
  });
</script>

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <script src="lib/js/jquery.min.js"></script>
  <script src="lib/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/js/jquery.mask.min.js"></script>
  <script src="lib/js/sb-admin.min.js"></script>

</body>

</html>
