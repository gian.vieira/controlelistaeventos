<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Cadastro de Eventos</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url() ?>application/views/lib/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="<?= base_url() ?>application/views/lib/css/sb-admin.css" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="inscricoes">Eventos</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>


    
  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Eventos</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <a class="dropdown-item" href="<?= base_url('novoevento') ?>">Novo Evento</a>
          <a class="dropdown-item" href="<?= base_url('listarevento') ?>">Listar Eventos</a>
          <a class="dropdown-item" href="<?= base_url('inscricoes') ?>">Inscrições</a> 
          <a class="dropdown-item" href="<?= base_url('meuseventos')?>">Meus Eventos</a>        
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Usuários</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <a class="dropdown-item" href="<?= base_url('novousuario') ?>">Novo Usuário</a>
          <a class="dropdown-item" href="<?= base_url('listarusuario') ?>">Listar Usuários</a>
        </div>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('deslogar') ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Logout</span>
        </a>
      </li>   
    </ul>

    <div id="content-wrapper">

      <div class="container-fluid">
        <h3>Atualização de Usuários</h3>
        <form method="post" action="<?= base_url('usuario/atualizarusuario') ?>">
          <div class="form-group row">
            <label class="col-3 form-label">Nome</label>
            <div class="col-8">
              <input type="text" class="form-control" name="nome" placeholder="Nome do usuário" value = "<?php echo $usuarios['usuario']->usu_nome ?>" required>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-3 form-label" >Tipo</label>
            <div class="col-8">
                <option value="1">Administrador</option>
                <option value="2">Usuário</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
          <input type="hidden" class="form-control" name="codigo"  value = "<?php echo $usuarios['usuario']->usu_codigo ?>" required>

            <label class="col-3 form-label">CPF</label>
            <div class="col-8">
              <input type="text" class="form-control" name="cpf" placeholder="Digite o cpf" value = "<?php echo $usuarios['usuario']->usu_cpf ?>" required>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-3 form-label">Senha</label>
            <div class="col-8">
              <input type="password" class="form-control" placeholder="Digite a senha" name="senha" value = "<?php echo $usuarios['usuario']->usu_senha ?>" required>
            </div>
          </div>          
          <div class="form-group row">
            <label class="col-3 form-label">Telefone</label>
            <div class="col-8">
              <input type="text" class="form-control" name="telefone" placeholder="Numero do telefone" value = "<?php echo $usuarios['usuario']->usu_telefone ?>" required>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-3 form-label">Data de nascimento</label>
            <div class="col-8">
              <input type="date" class="form-control" name="data" placeholder="Digite a data de nascimento" value = "<?php echo $usuarios['usuario']->usu_datanascimento ?>" required>
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success">Atualizar</button>
          </div>
        </form>
        
      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Gerenciador de Eventos 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <script src="<?= base_url() ?>application/views/lib/js/jquery.min.js"></script>
  <script src="<?= base_url() ?>application/views/lib/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url() ?>application/views/lib/js/jquery.mask.min.js"></script>
  <script src="<?= base_url() ?>application/views/lib/js/sb-admin.min.js"></script>
</body>

</html>
