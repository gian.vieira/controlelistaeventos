<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url() ?>application/views/lib/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="<?= base_url() ?>application/views/lib/css/sb-admin.css" rel="stylesheet">

</head>

<body class="bg-dark">

  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Registrar-se</div>
      <div class="card-body">
        <form method="post" action="inserirRegistre">
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" name="nome" class="form-control" placeholder="Digite seu nome" required="required" autofocus="autofocus">
              <label for="usuario">Nome</label>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input type="password" name="senha" class="form-control" placeholder="Digite sua senha" required="required">
              <label for="senha">Senha</label>
            </div>
          </div>          
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" name="cpf" class="form-control" placeholder="Digite seu cpf" required="required">
              <label for="cpf">CPF</label>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input type="text" name="telefone" class="form-control" placeholder="Digite seu telefone" required="required">
              <label for="telefone">Telefone</label>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input type="date" name="data" class="form-control" placeholder="Digite sua data de nascimento" required="required">
              <label for="dataNasc">Data de Nascimento</label>
            </div>
          </div>
          <button type="submit" class="btn btn-success">Registre-me</button>
        </form>
      </div>
    </div>
  </div>

  <script src="<?= base_url() ?>application/views/lib/js/jquery.min.js"></script>
  <script src="<?= base_url() ?>application/views/lib/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url() ?>application/views/lib/js/jquery.mask.min.js"></script>
  <script src="<?= base_url() ?>application/views/lib/js/sb-admin.min.js"></script>

</body>

</html>
