<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function inserir($dados)
	{
		return $this->db->insert('usuario', $dados);
	}

	public function atualizar($dados)
	{
		$this->db->where('usu_codigo', $dados['usu_codigo']);
		return $this->db->update('usuario', $dados);
	}
	

	public function listar()
	{
		return $this->db->get('usuario');
	}

	public function tabelaUsuario()
	{
		
		$usuarios = $this->listar()->result();

		$opcoes = "";
		foreach ($usuarios as $usuario) {
			$opcoes .= "<tr>
						<td width='6'> {$usuario->usu_codigo}</td>
						<td> {$usuario->usu_nome}</td>
						<td> {$usuario->usu_cpf}</td>
						<td> {$usuario->usu_telefone}</td>
						<td> {$usuario->usu_datanascimento}</td>
						<td> {$usuario->usu_tipo}</td>
						<td> <a href='alterarusuario/$usuario->usu_codigo' button class='btn btn-success btn-block' value='{$usuario->usu_codigo}' name='codigoaltera' >Atualizar/Detalhar</a>
						</tr>".PHP_EOL;
		}

		return $opcoes;
	}
	
	public function selecionaUsuario($codigo = null)
	{
		$this->db->where('usu_codigo', $codigo);
		$dados['usuario'] = $this->db->get('usuario')->row();
		
		return $dados;
	}		
}
