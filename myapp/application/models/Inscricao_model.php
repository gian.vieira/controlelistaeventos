<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inscricao_model extends CI_Model {


    public function listar()
	{
		return $this->db->get('evento');
    }
    
    public function tabelaEvento()
	{
		
		$eventos = $this->listar()->result();

		$opcoes = "";
		foreach ($eventos as $evento) {
			$opcoes .= "<tr>
						<td> {$evento->eve_nome}</td>
						<td> {$evento->eve_data}</td>
						<td> {$evento->eve_local}</td>
						<td> R$ {$evento->eve_ingresso}</td>
						<td> <a href='inscrever/$evento->eve_codigo/{$this->session->userdata('usuario')} 'button class='btn btn-success btn-block' value='{$evento->eve_codigo}' name='codigoaltera' >Fazer Inscrição</a></td>
						
						</tr>".PHP_EOL;
		}

		return $opcoes;
    }
    
    public function inserirUsuarioEvento($codigo1=null, $codigo2=null)
    {
        $dados['ins_codigo_evento'] = $codigo1;
        $dados['ins_codigo_usuario'] = $codigo2;
        $this->db->insert('inscricao',$dados);
    }
}