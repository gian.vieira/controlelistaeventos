<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evento_model extends CI_Model {

    public function inserir($dados)
	{
		return $this->db->insert('evento', $dados);
	}

	public function listar()
	{
		return $this->db->get('evento');
	}

	public function atualizar($dados)
	{
		$this->db->where('eve_codigo', $dados['eve_codigo']);
		return $this->db->update('evento', $dados);
	}

	public function tabelaEvento()
	{
		
		$eventos = $this->listar()->result();

		$opcoes = "";
		foreach ($eventos as $evento) {
			$opcoes .= "<tr>
						<td width='6'> {$evento->eve_codigo}</td>
						<td> {$evento->eve_nome}</td>
						<td> {$evento->eve_local}</td>
						<td> {$evento->eve_tipo}</td>
						<td> {$evento->eve_data}</td>
						<td> R$ {$evento->eve_ingresso}</td>
						<td> <a href='alterarevento/$evento->eve_codigo'button class='btn btn-success btn-block' value='{$evento->eve_codigo}' name='codigoaltera' >Atualizar/Detalhar</a></td>
						
						</tr>".PHP_EOL;
		}

		return $opcoes;
	}

	public function listarInscritos()
	{
		$this->db
		->select('eve_nome, eve_data')
		->from('evento')
		->join('inscricao', 'eve_codigo = ins_codigo_evento')
		->join('usuario', 'usu_codigo = ins_codigo_usuario')
		->where('ins_codigo_usuario',$this->session->userdata('usuario'));
		return $this->db->get();
	}
		
	public function tabelaEventoInscrito()
	{
		
		$eventos = $this->listarInscritos()->result();

		$opcoes = "";
		foreach ($eventos as $evento) {
			$opcoes .= "<tr>
						
						<td> {$evento->eve_nome}</td>
						<td> {$evento->eve_data}</td>
						
						
						</tr>".PHP_EOL;
		}

		return $opcoes;
	}

	public function selecionaevento($codigo = null)
	{
		$this->db->where('eve_codigo', $codigo);
		$dados['evento'] = $this->db->get('evento')->row();
		
		return $dados;
	}	

	public function listaUnicoEvento($codigo = null)
	{
		$this->db->select('eve_nome');		
		$this->db->where('eve_codigo', $codigo);
		$retorno = $this->db->get('evento')->result();
		

		
		return $retorno;
	}

}