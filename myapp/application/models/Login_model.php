<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	
	function validar()
	{

		$dados['usu_cpf'] = $this->input->post("cpf");
		$dados['usu_senha'] = $this->input->post("senha");	
		$this->db->where("usu_cpf", $dados['usu_cpf']);
		$this->db->where("usu_senha", $dados['usu_senha']);
		$resultado['resultados'] = $this->db->get('usuario')->result();
		if(empty($resultado['resultados']))
		{
			$_SESSION['login_erro'] = "Usuário ou senha inválidos";
			$this->session->unset_userdata('logado');
			return false;
		}		
		else
			$dados['usuario'] = $resultado['resultados'][0]->usu_codigo;
			$dados['nome'] = $resultado['resultados'][0]->usu_nome;
			$this->session->set_userdata($dados);
			return true;     	
	}

	function destroi_sessao()
	{
		$this->session->unset_userdata('usuario');
	}
}
