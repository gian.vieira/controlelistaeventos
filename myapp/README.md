# ControleListaEventos

### Descrição do Projeto

Sistema para Controle de Listas de Evento
O sistema controla a lista de ingressantes em eventos. Sejam festas, eventos beneficentes, etc. 
Organizadores controlam a entrada de usuários na lista, assim como organizam os descontos para cada tipo de ingresso. 
Geram relatórios de usuários que pertencem a lista do evento. Usuários em geral podem se cadastrar e entrar em listas de eventos.

**Responsável**: Matheus


### Modelagem

Administrador acessa o sistema de gerenciamento de eventos.
Administrador pode criar, alterar e deletar um evento.
Ao criar um evento, o Administrador deve preencher o nome do evento, tipo, data, local e valor de ingresso.

O sistema deve ter uma tela em que os usuários consigam localizar os eventos em aberto.
O sistema deve permitir que os usuários façam um cadastro de usuário.
O sistema deve permitir que os usuários consigam fazer a inscrição nos eventos de seu interesse.

Após fazer o cadastro, a solicitação fica pendente até que um Administrador aprove a solicitação.
O Administrador pode gerar um relatório dos usuários por evento.


Modelo de Dados

Usuário

-Nome
-CPF
-Telefone
-Data de nascimento
-Tipo (Administrador/usuário)

Evento

-Nome
-Tipo
-Data
-local
-Ingresso

Inscrições

-Cod_Evento
-Cod_Usuário
-Status (Aprovado/Reprovado)

**Responsável**: Tiago

### Prototipagem

**Responsável**: Gian
